#!/usr/bin/env python

from os import path
from subprocess import check_call

import arg_parser
import context
from helpers import utils


def main():
    args = arg_parser.sender_first()

    cc_repo = path.join(context.third_party_dir, 'pqsa')
    send_src = path.join(cc_repo, 'server', 'pqsa_server')
    recv_src = path.join(cc_repo, 'client', 'pqsa_client')

    if args.option == 'sender':
        cmd = [send_src, '-p', args.port, '-t', '75']
        check_call(cmd, cwd=utils.tmp_dir)
        return

    if args.option == 'receiver':
        cmd = [recv_src, args.ip, '-p', args.port]
        check_call(cmd, cwd=utils.tmp_dir)
        return


if __name__ == '__main__':
    main()
