# PQSA
OS ubuntu 18

## Install pantheon
### Preparation
  git clone https://github.com/StanfordSNR/pantheon.git

  git submodule update --init --recursive

### Dependencies
  tools/install_deps.sh

  src/experiments/setup.py --install-deps --all

### Setup
  src/experiments/setup.py --setup --all

## Testing
### Running the Pantheon
To test schemes in emulated networks locally, run

  src/experiments/test.py local --all --data-dir DIR

To test schems over the Internet to remote machine, run

  src/experiments/test.py remote --all --data-dir DIR
### Pantheon analysis
To analyze test result, run
  src/analysis/analyze.py --data-dir DIR

## Add PQSA
### Configure PQSA
  git submodule add https://github.com/ATCP/PQSA-Emulator.git third_party/pqsa

  vim .gitmodules
add ignore = dirty
  copy third_party/pqsa/backup/pqsa.py ./src/wrappers
  copy third_party/pqsa/backup/config.yml ./src/config.yml
  copy third_party/pqsa/backup/.travis.yml ./travis.yml

### Run PQSA
  src/experiments/test.py local --schemes pqsa --data-dir DIR
  src/analysis/analyze.py --data-dir DIR

### Reference


